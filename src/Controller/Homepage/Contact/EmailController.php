<?php

namespace App\Controller\Homepage\Contact;

use App\Entity\Emails;
use App\Form\Emails\EmailFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EmailController extends AbstractController
{
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ){
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/send", name="send_email", methods={"GET", "POST"})
     */
    public function SendEmail(Request $request){
        $form = $this->createForm(EmailFormType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $formData = $form->getData();

            $newMail = new Emails(
                $formData['email'],
                $formData['title'],
                $formData['description']
            );
        }
    }
}