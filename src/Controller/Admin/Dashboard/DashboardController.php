<?php

namespace App\Controller\Admin\Dashboard;

use App\Repository\PostsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    private $postsRepository;
    private $entityManager;

    public function __construct(
        PostsRepository $postsRepository,
        EntityManagerInterface $entityManager
    ){
        $this->postsRepository = $postsRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/admin", name="adminHomepage")
     */
    public function Homepage(){
        return $this->render('adminBase.html.twig');
    }
}