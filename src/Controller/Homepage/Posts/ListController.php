<?php

namespace App\Controller\Homepage\Posts;

use App\Repository\PostsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    private $postsRepository;
    private $entityManager;

    public function __construct(
        PostsRepository $postsRepository,
        EntityManagerInterface $entityManager
    ){
        $this->postsRepository = $postsRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="homepage", methods={"GET"})
     */
    public function Homepage(){
        $posts = $this->postsRepository->findBy([], [
            'id' => 'DESC'
        ]);

        return $this->render('homepage.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * @Route("/kontakt", name="contact", methods={"GET"})
     */
    public function Contact(){
        return $this->render('anon/Contact/contact.html.twig');
    }
}