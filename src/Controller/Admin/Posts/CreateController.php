<?php

namespace App\Controller\Admin\Posts;

use App\Entity\Posts;
use App\Form\Posts\PostCreateType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CreateController extends AbstractController
{
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ){
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @Route("/new", name="post_create", methods={"GET", "POST"})
     */
    public function CreatePost(Request $request){
        $form = $this->createForm(PostCreateType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $formData = $form->getData();

            $newPost = new Posts(
                $formData['title'],
                $formData['description']
            );

            $this->entityManager->persist($newPost);
            $this->entityManager->flush();

            $this->addFlash('success', 'Post został opublikowany');
            return $this->redirectToRoute('homepage');
        }

        return $this->render('admin\Posts\CreatePost.html.twig', [
            'form' => $form->createView()
        ]);
    }
}